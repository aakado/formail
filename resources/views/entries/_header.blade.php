<header class="container d-flex justify-content-between mb-4">
    <p><a href="/entries"
          class="text-decoration-none text-dark">My Entries</a> / {{$entry->name}}</p>
</header>
