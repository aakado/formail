@extends('layouts.app')

@section('content')
    @include('entries._header')
    <main>
        <div class="container">
            <div class="row justify-content-center">
                <form action="/entries" method="POST">
                    @csrf
                    @method('POST')
                    @include('entries._form')
                    <button type="submit" class="btn btn-primary">Create</button>
                    <a href="/entries">
                        <button type="button" class="btn btn-secondary">Cancel</button>
                    </a>
                </form>
            </div>
        </div>
    </main>
@endsection
