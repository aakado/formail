<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;
use Illuminate\Support\Collection;

class EntryBelongsToUser implements Rule
{
    /**
     * @var string
     */
    private string $table;

    /**
     * Create a new rule instance.
     *
     * @param string $table
     */
    public function __construct(string $table)
    {
        $this->table = $table;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param string $attribute
     * @param mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $property = $this->table;
        if (isset(auth()->user()->$property)) {
            if (is_array($value) || $value instanceof Collection) {
                foreach ($value as $valueItem) {
                    return auth()->user()->$property->contains($valueItem);
                }
            } else {
                return auth()->user()->$property->contains($value);
            }
        } else {
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return trans('validation.entry_belongs_to_user');
    }
}
