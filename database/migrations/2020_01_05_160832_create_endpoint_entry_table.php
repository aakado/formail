<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEndpointEntryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('endpoint_entry', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('endpoint_id')->unsigned();
            $table->integer('entry_id')->unsigned();
            $table->timestamps();

            $table->foreign('endpoint_id')->references('id')->on('endpoints');
            $table->foreign('entry_id')->references('id')->on('entries');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('endpoint_entry');
    }
}
