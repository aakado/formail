# Dokumentation
## Allgemein
### Eigene Funktionen gemäss Projektantrag
Die Grundfunktionen wurden bis auf eine Ausnahme fertig und funktionstüchtig implementiert.
Die Endpunkte funktionieren und das Rate-Limiting auch, jedoch werden noch keine Daten per Mail versendet.
Die Zusatzfunktionen für Administratoren wurden funktionstüchtig implementiert.

## Sicheres Sessionhandling
> Funktionen, welche nur bestimmte Benutzer / Benutzergruppen ausführen dürfen, sind sowohl im Frontend wie auch im Backend berücksichtigt und funktionieren

Nur der Administrator hat Zugriff auf die Benutzerverwaltung. Normale Benutzer bekommen einen 403, wenn sie versuchen, darauf zuzugreifen. Zudem wird der Link auch nur bei Admins im Frontend angezeigt.


> Der Login-Mechanismus ist sicher implementiert, Passwörter sind gehashed und gesalzen gespeichert

Der Mechanismus wird schon komplett und sicher von Laravel mitgeliefert. Die Passwörter sind gehasht und gesalzen abgespeichert.
Mehr dazu findet man hier: https://laravel.com/docs/6.x/authentication

> Das Problem der Session-Fixation wurde in der Lösung berücksichtigt und abgesichert

Auch das Session Handling wird von Laravel standardmässig berücksichtigt: 
Mehr dazu findet man hier: https://laravel.com/docs/6.x/authentication

## Absicherung Standardangriffe
> Injections werden durch entsprechende Gegenmassnahmen verhindert

Das ORM von Laravel bietet automatischen Schutz vor SQL Injection: https://laravel.com/docs/5.8/queries#introduction
Die Rechte des DB Benutzer beschränken sich auf den vollen Zugriff des aktuellen Schemas. Weitere Einschränkungen sind für die Entwicklung mit Laravel nicht optimal.

> XSS-Angriffe werden durch entsprechende Gegenmassnahmen verhindert

Blade übernimmt Output-Escaping: https://laravel.com/docs/6.x/blade

## Errorhandling
> Der Benutzer erhält keine Fehlermeldungen, welche auf Applikationsinterna schliessen lassen. Dies erfordert ein adäquates Errorhandling und Logging

Laravel versteckt die Errormeldungen automatisch in produktiv umgebungen.

## Rate Limiting
Das Rate limiting wurde wie im Projektantrag beschrieben implementiert und funktioniert.
