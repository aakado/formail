# Deployment

You can deploy this application in very few steps with [Docker Swarm mode](https://docs.docker.com/engine/swarm/) and [traefik](https://containo.us/traefik/).

## Requirements

The following tools are required for a proper deployment:

- Docker with [Docker Swarm mode](https://docs.docker.com/engine/swarm/) enabled.
- [traefik](https://containo.us/traefik/) with a minimum version of v2.0
    - The traefik should be configured to get [let's encrypt](https://letsencrypt.org/) certificates automatically

!!! attention "Allow SMTP ports for outgoing requests"
    It's very important to allow outgoing requests on common SMTP port like 25, 2525, 465, 587.
    But you also have to make sure, that a malicious user can't connect to any other server in your current infrastructure.


## Deploy

First of all, create the following file: `docker-compose.yml` and paste the

Run the following lines to create a new docker-compose.yml file:
```sh
curl https://gitlab.com/aakado/formail/raw/master/docker-compose.prod.yml > docker-compose.yml
```

Change the default values:  
Replace `formail.dev` with your domain everywhere in the `docker-compose.yml` file.

Set the environment variables:  
Inspect all environment variables in the `docker-compose.yml` file and change them according to your needs.  

### App Key

!!! danger
    You have to generate a new APP_KEY for security reasons. 
    Run `php artisan key:generate --show` to generate a new one and copy the output.
    If you don't have laravel running locally or in a docker container, you can get a key with the following command instead:
    `docker run --entrypoint php registry.gitlab.com/aakado/formail:dev-latest artisan key:generate --show`.

### `.env` files

??? hint "Put the environment variables into a separated `.env` file"
    If you do not want to put your environment variables directly into the `docker-compose.yml` file, you can also put them into a separate `.env` file as key-value pairs.
    
    First create a copy of the .env.example file:
    ```sh
    curl https://gitlab.com/aakado/formail/raw/master/.env.example > .env
    ``` 
    
    Then you have to link the file to the service like this: 
    ```yaml
    env_file:
      - .env
    ```
    You can do the same with the database environment variables. But you should use a separate file like `.db.env`.

Now you can deploy the stack:
```sh
docker stack deploy -c docker-compose.yml formail
```

