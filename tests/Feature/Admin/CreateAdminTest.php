<?php

namespace Tests\Feature\Admin;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Hash;
use Tests\TestCase;

class CreateAdminTest extends TestCase
{
    use RefreshDatabase;

    /**
     * A user can be admin.
     *
     * @return void
     */
    public function testUserCanBeAdmin()
    {
        $user = $this->signIn(factory(User::class)->create([
            'admin' => true
        ]));
        $this->assertTrue($user->admin);
    }


    /**
     * An admin user can be created with the admin:create artisan command
     *
     * @return void
     */
    public function testAdminCanBeCreatedWithConsole()
    {
        $name = 'admin';
        $email = 'admin@test.example';
        $password = 'password';

        $user = [
            'name' => $name,
            'email' => $email,
        ];

        $this->artisan('admin:create')
            ->expectsQuestion('What is your username?', $name)
            ->expectsQuestion('What is your email address?', $email)
            ->expectsQuestion('What is your password?', $password)
            ->expectsQuestion('Repeat your password', $password)
            ->expectsOutput('Successfully created admin account')
            ->assertExitCode(0);

        $this->assertDatabaseHas('users', $user);

        $insertedUser = User::where($user)->first();
        $this->assertTrue(Hash::check($password, $insertedUser->password));
    }

    /**
     * The input has to be validated before the admin is created.
     */
    public function testCreateAdminInputIsValidated()
    {
        $name = '';
        $email = '';
        $password = 'password';
        $password_confirmation = 'bla';

        $this->artisan('admin:create')
            ->expectsQuestion('What is your username?', $name)
            ->expectsQuestion('What is your email address?', $email)
            ->expectsQuestion('What is your password?', $password)
            ->expectsQuestion('Repeat your password', $password_confirmation)
            ->expectsOutput('Invalid input parameters: name, email, password')
            ->assertExitCode(1);
    }


}
