<?php

namespace Tests\Unit;

use App\Credential;
use App\Rules\EntryBelongsToUser;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class EntryBelongsToUserRuleTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Valid keywords should pass the rule
     *
     * @return void
     */
    public function testValidEntryPassesRule()
    {
        $user = $this->signIn();
        $credential = factory(Credential::class)->create([
            'user_id' => $user
        ]);
        $rule = new EntryBelongsToUser('credentials');
        $this->assertTrue($rule->passes('credential_id', $credential->id));
    }

    /**
     * Invalid keywords should fail the rule
     *
     * @return void
     */
    public function testInvalidEntryFailsRule()
    {
        $this->signIn();
        $credential = factory(Credential::class)->create();
        $rule = new EntryBelongsToUser('credentials');
        $this->assertFalse($rule->passes('credential_id', $credential->id));
    }

    /**
     * Valid keywords should pass the rule. Test with array
     *
     * @return void
     */
    public function testValidEntryArrayPassesRule()
    {
        $user = $this->signIn();
        $credential = factory(Credential::class, 3)->create([
            'user_id' => $user
        ]);
        $rule = new EntryBelongsToUser('credentials');
        $this->assertTrue($rule->passes('credential_id', $credential->map(fn($credential) => $credential->id)));
    }

    /**
     * Invalid keywords should fail the rule. Test with array
     *
     * @return void
     */
    public function testInvalidEntryArrayFailsRule()
    {
        $this->signIn();
        $credential = factory(Credential::class, 3)->create();
        $rule = new EntryBelongsToUser('credentials');
        $this->assertFalse($rule->passes('credential_id', $credential->map(fn($credential) => $credential->id)));
    }

}
